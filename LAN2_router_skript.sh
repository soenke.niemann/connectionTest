#!/bin/bash


row=$(speedtest | grep -E "Download|Upload|Hosted by")



down="${row%U*}"
down="${down##*D}"
down="D$down"
down=$(echo $down | tr ":" ";")
down=$(echo $down | tr "." ",")

up="${row##*U}"
up="U$up "
up=$(echo $up | tr ":" ";")
up=$(echo $up | tr "." ",")

ping="${row%D*}"
ping=$(echo $ping | tr ":" ";")
ping=$(echo $ping | tr "." ",")


final="$(date);      $down;      $up;      $ping"

echo $final
echo $final >> /home/soenke/testInternet/LAN2_router_messreihe.csv

cd /home/soenke/testInternet

git add LAN2_router_messreihe.csv
git commit LAN2_router_messreihe.csv -m "update"
git pull
git push
