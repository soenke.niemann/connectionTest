#!/bin/bash


row=$(speedtest | grep -E "Download|Upload|Hosted by")



down="${row%U*}"
down="${down##*D}"
down="D$down"
down=$(echo $down | tr ":" ";")
down=$(echo $down | tr "." ",")

up="${row##*U}"
up="U$up "
up=$(echo $up | tr ":" ";")
up=$(echo $up | tr "." ",")

ping="${row%D*}"
ping=$(echo $ping | tr ":" ";")
ping=$(echo $ping | tr "." ",")


final="$(date);      $down;      $up;      $ping"

echo $final
echo $final >> /home/soenke/connectionTest/LAN1_powerline_messreihe.csv

cd /home/soenke/connectionTest

git add LAN1_powerline_messreihe.csv
git commit LAN1_powerline_messreihe.csv -m "update"
git pull
git push
git pull

