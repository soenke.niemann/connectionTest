# Check and log your internet speed

I created this script, because I wanted to have proof, that the internet 
in my flat tended to be very poor.

This bash script uses the **speedtest-cli** from the popular speedtest.net
internet speed test service to log download and upload speed as well as
the ping.

You can install it on ubuntu via `sudo apt-get install speedtest-cli`.
After installing you can run it in shell with 
`speedtest-cli` or just `speedtest`.

The **output** of the speedtest gets fed into a pipe for a grep command, 
which then in the following seperates and cleans up the output.
The cleaned up data then finally gets written into an **.csv file.**